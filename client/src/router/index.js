import Vue from 'vue'
import Router from 'vue-router'
// import routes from '@/routes'
import Start from '@/components/Start.vue'
import Registration from '@/components/Registration.vue'
import SignIn from '@/components/SignIn.vue'
import QuestsPage from '@/components/QuestsPage.vue'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Start Page',
      component: Start
    },
    {
      path: '/registration',
      name: 'Registration',
      component: Registration
    },
    {
      path: '/signin',
      name: 'Sign In',
      component: SignIn
    },
    {
      path: '/questspage',
      name: 'Quests Page',
      component: QuestsPage
    }
  ]
})

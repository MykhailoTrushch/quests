import api from '@/services/api'

export default {
  fetchQuests () {
    return api().get('quests')
  }
}

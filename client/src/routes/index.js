import Start from '@/components/Start'
import QuestsPage from '@/components/QuestsPage'

const routes = [
  {
    path: '/start',
    name: 'Start',
    component: Start
  },
  {
    path: '/',
    name: 'QuestsPage',
    component: QuestsPage
  }
]

export default routes

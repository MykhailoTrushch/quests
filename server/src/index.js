const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const config = require('./config/config')

const app = express()

app.use(bodyParser.json())
app.use(morgan('combined'))
app.use(cors())

app.listen(process.env.PORT || config.port,
   ()=>console.log('Server on port %s...', config.port))

app.get('/quests', (req, res)=>{
  res.send(
    [{
      title:"Hello",
      description:"Hi there! How are you?"
    },
    {
      title:"World!",
      description:"Hi there! How are you?"
    }]
  )
})
